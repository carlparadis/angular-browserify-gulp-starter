### NOTE: This hasn't been maintained since forking to webpack: [https://bitbucket.org/carlparadis/angular-webpack-sass-gulp-starter](https://bitbucket.org/carlparadis/angular-webpack-sass-gulp-starter) ###

# Starter Angular/Browserify/Less/Gulp app to consume JSON api. #

## Getting Started

Make sure you have Node (and NPM) and Bower installed.

`npm install -g bower` - This will install bower globally, so that you can run the postinstall script in the package.json

Install all dependencies (node, bower, gulp, etc...):

* `npm install`

Then, run gulp to start up the local server with BrowserSync:

* `npm run g:mb` - NOTE: This runs 'gulp --mockBackend', *with* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API.
* `npm run g:buildmb` - NOTE: This runs 'gulp build --mockBackend', *without* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API.

Other Gulp build options:

* `npm run g` - This runs gulp *with* Watchers and BrowserSync active, without args (no mocks)
* `npm run g:build` - This runs gulp build *without* Watchers and BrowserSync active, without args (no mocks)
* `npm run g:clean` - This runs gulp clean, which clears the `*/dist` directory
* `npm run g:mob` - This runs gulp clean, which clears the `*/dist` directory
* `npm run g:mbmob` - NOTE: This runs 'gulp --mockBackend --mobile', *with* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API, '--mobile' runs mobile specific tasks.
* `npm run g:buildmbmob` - NOTE: This runs 'gulp build --mockBackend --mobile', *without* BrowserSync and Watchers active, the '--mockBackend' option is necessary without an actual API, '--mobile' runs mobile specific tasks.

Adding extra Node ENV parameters that the Gulp build listens for:

* `npm run g:build -- --serverId=prod` - NOTE: The extra serverId parameter identifies that the build is for production, current potential values are 'prod', 'test', 'dev'
* `npm run g:mb -- --noCacheBreak` - NOTE: The extra noCacheBreak parameter prevents an appending of cache break parameters inside the app's index.html
* Example of combined extra parameters `npm run g:mb -- --serverId=test --noCacheBreak`


For dev, Navigate to [http://localhost:9000/mock.html](http://localhost:9000/mock.html) for BrowerserSync Mode