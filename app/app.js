/* global angular, module, document, require */
'use strict';

var _ = require('lodash'),
    uriConfig = require('./config/uri'),
    routerConfig = require('./config/route'),
    translationConfig = require('./config/translation');

// custom components/directives
var templates = require('./config/templates');

// states/modules
var mainModule = require('./modules/main/main');
var demoModule = require('./modules/demo/demo');

// dependency array
var dependencies = [
    // external
    'ui.router', 'ngAnimate', 'ngSanitize', 'ngMessages', 'ngAria', 'ngStorage',
    'pascalprecht.translate',

    // templates
    'app.templates',

    // components/directives

    // states/modules
    mainModule.name, demoModule.name
];

angular.module('app', dependencies)
    .constant('uriConfig', uriConfig)
    .config(routerConfig)
    .config(translationConfig);
