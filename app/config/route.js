/* global angular, module, require */
'use strict';

/**
 *
 */
module.exports = ['$stateProvider','$urlRouterProvider', 'DEMO_TEMPLATE_URL',
      function($stateProvider, $urlRouterProvider, DEMO_TEMPLATE_URL) {

  $urlRouterProvider.otherwise("/demo");

  $stateProvider

    .state("demo", {
      url: "/demo",
        templateUrl: DEMO_TEMPLATE_URL,
      controller: 'DemoController as demoCtrl',
      resolve: {
        demoApiData: function (DemoApiService) {
          return DemoApiService.getDemoResults();
        }
      }
    });

}];