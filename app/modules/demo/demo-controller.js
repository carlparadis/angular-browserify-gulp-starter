/* global angular, module, require */
'use strict';

/**
 *
 */
module.exports = ['demoApiData',
    function (demoApiData) {

        var demoController = this;

        console.log("demoApiData", demoApiData);

        demoController.demoData = demoApiData;
    }];