/* global angular, module, require */
'use strict';

var demoTemplateUrl = 'demo/demo.html',
    demoController = require('./demo-controller'),
    demoApiService = require('./demo-api-service');

module.exports = angular.module('app.demo', [])
    .constant('DEMO_TEMPLATE_URL', demoTemplateUrl)
    .controller('DemoController', demoController)
    .service('DemoApiService', demoApiService);
