/* global angular, module, require */
'use strict';

var mainController = require('./main-controller');

module.exports = angular.module('app.main', [])
    .controller('MainController', mainController);
