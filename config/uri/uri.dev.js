/* global angular, module, require */
'use strict';

/**
 * Configure urls used throughout the site
 *
 */
module.exports = {
  'BASE_SERVICE_URL' : 'http://localhost:9000/api'
};